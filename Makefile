PACKAGE=iso-latin-1-8x16
VERSION=0.0.1
#RELEASE=1
CWD=$(CURDIR)
DESTARCH=$(CWD)/$(PACKAGE)-$(VERSION)
#RELEASE=$(shell date +%s)

all: psf psfu psfugz

psf:
	@mkdir -p $(DESTARCH)
	@txt2psf $(PACKAGE).txt $(DESTARCH)/$(PACKAGE).psf

psfu: psf
	@psfaddtable $(DESTARCH)/$(PACKAGE).psf $(PACKAGE).table $(DESTARCH)/$(PACKAGE).psfu
	@rm $(DESTARCH)/$(PACKAGE).psf

psfugz: psfu
	@gzip $(DESTARCH)/$(PACKAGE).psfu

install:
	@cp $(DESTARCH)/$(PACKAGE).psfu.gz /usr/share/consolefonts/$(PACKAGE).psfu.gz

clean:
	@if [ -d $(DESTARCH) ]; then echo "About to remove $(DESTARCH) in 5 seconds ..."; sleep 5; fi
	@rm -rf $(DESTARCH)

.PHONY:
	clean
	all